
@tag
Feature: Admin hospiotal

  @AddDoctor
  Scenario: Add Doctor
    Given Add new doctor
    When Add new register doctor in admin hospital
    |name|lastname|telephone|typedocumente       |id       |
    |Juan|Sanchez |123971237|Cédula de ciudadanía|947359307| 
    Then succes register doctor
    

   @AddPatient
   Scenario: Add patient 
    Given Add new patient
    When Add new register patient in admin hospital 
    |name   |lastname|telephone|typedocumente       |id        |
    |Gustavo|Simosa  |123971237|Cédula de ciudadanía|1127598890|  
     Then succes register patient
 
  @Addappointment
  Scenario: Add appointment
    Given Add new  appointment 
    When Add new register appointment in admin hospital 
    |DayAppointment   | IdentificationPatient | IdentificationDoctor | Observation       |
    |03/14/2019       | 1127598890            | 947359307            | puntual           |
  Then succes register appointment