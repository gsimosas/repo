package com.choucair.formacion.definition;


import java.util.List;

import com.choucair.formacion.steps.AddPatientValidationSteps;
import com.choucair.formacion.steps.AdminHospitalValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AddPatientValidationDefinition {

	@Steps
	AdminHospitalValidationSteps adminHospitalValidationSteps;
	
	@Steps
	AddPatientValidationSteps addPatientValidationSteps;
	
	
	@Given("^Add new patient$")
	public void add_new_patient() {
	adminHospitalValidationSteps.formValidation();
	addPatientValidationSteps.form_add_Patient();
	}

	@When("^Add new register patient in admin hospital$")
	public void add_new_register_patient_in_admin_hospital(DataTable dtDatosForm) throws InterruptedException {
		  List<List<String>> data = dtDatosForm.raw();
			Thread.sleep(5000);
			for (int i = 1; i < data.size(); i++) {
				addPatientValidationSteps.form_add_Patient_data_table(data, i);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				}
			}
		}	
	
	

	@Then("^succes register patient$")
	public void succes_register_patient() {

	}


	
}
