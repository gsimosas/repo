package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.AddAppointmentValidationSteps;

import com.choucair.formacion.steps.AdminHospitalValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AddAppointmentDefinition {
	
	@Steps
	AdminHospitalValidationSteps adminHospitalValidationSteps;
	
	@Steps
	AddAppointmentValidationSteps addAppointmentValidationSteps;
	
	@Given("^Add new  appointment$")
	public void add_new_appointment()  {
     adminHospitalValidationSteps.formValidation(); 
     addAppointmentValidationSteps.form_add_Appointment();
	}

	@When("^Add new register appointment in admin hospital$")
	public void add_new_register_appointment_in_admin_hospital(DataTable dtDatosForm) throws InterruptedException {
		  List<List<String>> data = dtDatosForm.raw();
			Thread.sleep(5000);
			for (int i = 1; i < data.size(); i++) {
				addAppointmentValidationSteps.form_add_Appointment_data_table(data, i);
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				}
			}
		}	
		
	@Then("^succes register appointment$")
	public void succes_register_appointment(){

	}

	
	

}
