package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.AddDcotorValidationSteps;
import com.choucair.formacion.steps.AdminHospitalValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AdminHospitalValidationDefinition {
	
	@Steps
	AdminHospitalValidationSteps adminHospitalValidationSteps;
	
	@Steps
	AddDcotorValidationSteps addDcotorValidationSteps;
	
	@Given("^Add new doctor$")
	public void add_new_doctor()  {
		adminHospitalValidationSteps.formValidation();
		addDcotorValidationSteps.form_add_doctor();
	}

	@When("^Add new register doctor in admin hospital$")
	public void add_new_register_doctor_in_admin_hospital(DataTable dtDatosForm) throws InterruptedException {
		List<List<String>> data = dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i = 1; i < data.size(); i++) {
			addDcotorValidationSteps.form_add_doctor_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}

	@Then("^succes register doctor$")
	public void succes_register_doctor() {
	    
	}
	

	
}


