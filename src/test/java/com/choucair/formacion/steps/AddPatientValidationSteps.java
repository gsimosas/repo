package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AddPatientValidationPage;

import net.thucydides.core.annotations.Step;

public class AddPatientValidationSteps {

	AddPatientValidationPage addPatientValidationPage;
	
	@Step
	public void form_add_Patient_data_table(List<List<String>>data, int id) {
		addPatientValidationPage.Name(data.get(id).get(0).trim());
		addPatientValidationPage.Lastname(data.get(id).get(1).trim());
		addPatientValidationPage.Telephone(data.get(id).get(2).trim());
		addPatientValidationPage.Identificationtype(data.get(id).get(3).trim());
		addPatientValidationPage.Identification(data.get(id).get(4).trim());
		addPatientValidationPage.Health();
		addPatientValidationPage.Buttonsave();
	}
	
	@Step
	public void form_add_Patient () {
		addPatientValidationPage.menuAddPatient();
	}

}

	