package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AddAppointmentValidationPage;

import net.thucydides.core.annotations.Step;


public class AddAppointmentValidationSteps {

	AddAppointmentValidationPage addAppointmentValidationPage;
	
	
	@Step
	public void form_add_Appointment_data_table(List<List<String>>data, int id) throws InterruptedException {
		addAppointmentValidationPage.DayAppointment(data.get(id).get(0).trim());
		addAppointmentValidationPage.IdentificationPatient(data.get(id).get(1).trim());
		addAppointmentValidationPage.IdentificationDoctor(data.get(id).get(2).trim());
	    addAppointmentValidationPage.Observation(data.get(id).get(3).trim());
		addAppointmentValidationPage.Buttonsave();
	}
	
	@Step
	public void form_add_Appointment() {
		addAppointmentValidationPage.menuAddAppointment();	
	}
}