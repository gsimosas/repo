package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AddDoctorValidationPage;

import net.thucydides.core.annotations.Step;

public class AddDcotorValidationSteps {
	AddDoctorValidationPage addDoctorValidationPage;
	
	@Step
	public void form_add_doctor_data_table(List<List<String>>data, int id) {
		addDoctorValidationPage.Name(data.get(id).get(0).trim());
		addDoctorValidationPage.Lastname(data.get(id).get(1).trim());
		addDoctorValidationPage.Telephone(data.get(id).get(2).trim());
		addDoctorValidationPage.Identificationtype(data.get(id).get(3).trim());
		addDoctorValidationPage.Identification(data.get(id).get(4).trim());
		addDoctorValidationPage.Buttonsave();
	}
	
	@Step
	public void form_add_doctor () {
		addDoctorValidationPage.menuAddDoctor();
	}

}
