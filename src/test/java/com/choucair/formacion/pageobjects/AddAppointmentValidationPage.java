package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AddAppointmentValidationPage extends PageObject {

	// Menu Add Doctor
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[6]")
	public WebElement MenuAddAppointment;

	// Field Day
	@FindBy(xpath = "//*[@id=\'datepicker\']")
	public WebElementFacade txtDayAppointment;

	// Field IdentificationPatient
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
	public WebElementFacade txtIdentificationPatient;

	// Field IdentificationDoctor
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
	public WebElementFacade txtIdentificationDoctor;

	// Field Observation
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/textarea")
	public WebElementFacade txtObservation;

	// Field Button save
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
	public WebElementFacade btnSave;

	public void Menuvalidation() {
		MenuAddAppointment.click();
	}

	public void DayAppointment(String dataUser) throws InterruptedException {
		txtDayAppointment.sendKeys(dataUser);
		txtDayAppointment.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
	}

	public void IdentificationPatient(String dataUser) {
		txtIdentificationPatient.click();
		txtIdentificationPatient.clear();
		txtIdentificationPatient.sendKeys(dataUser);
	}

	public void IdentificationDoctor(String dataUser) {
		txtIdentificationDoctor.click();
		txtIdentificationDoctor.clear();
		txtIdentificationDoctor.sendKeys(dataUser);

	}

	public void Observation(String dataUser) {
		txtObservation.click();
		txtObservation.clear();
		txtObservation.sendKeys(dataUser);
	}

	public void Buttonsave() {
		btnSave.click();
	}

	public void menuAddAppointment() {
		MenuAddAppointment.click();
	}

}
