package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class AddDoctorValidationPage extends PageObject {

	// Menu Add Doctor
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[1]")
	public WebElement MenuAddDoctor;

	// Field Name
	@FindBy(xpath = "//*[@id=\'name\']")
	public WebElementFacade txtName;

	// Field Last Name
	@FindBy(xpath = "//*[@id=\'last_name\']")
	public WebElementFacade txtLastName;

	// Field Telephone
	@FindBy(xpath = "//*[@id=\'telephone\']")
	public WebElementFacade txtTelephone;

	// Field Identification Type
	@FindBy(xpath = "//*[@id=\'identification_type\']")
	public WebElementFacade cbmTypeIdentification;

	// Field Identification
	@FindBy(xpath = "//*[@id=\'identification\']")
	public WebElementFacade txtIdentification;

	// Field Button save
	@FindBy(xpath = "//*[@id=\'page-wrapper\']/div/div[3]/div/a")
	public WebElementFacade btnSave;

	public void Menuvalidation() {
		MenuAddDoctor.click();
	}

	public void Name(String dataUser) {
		txtName.click();
		txtName.clear();
		txtName.sendKeys(dataUser);

	}

	public void Lastname(String dataUser) {
		txtLastName.click();
		txtLastName.clear();
		txtLastName.sendKeys(dataUser);

	}

	public void Telephone(String dataUser) {
		txtTelephone.click();
		txtTelephone.clear();
		txtTelephone.sendKeys(dataUser);

	}

	public void Identificationtype(String dataUser) {
		cbmTypeIdentification.click();
		cbmTypeIdentification.selectByVisibleText(dataUser);

	}

	public void Identification(String dataUser) {
		txtIdentification.click();
		txtIdentification.clear();
		txtIdentification.sendKeys(dataUser);

	}

	public void Buttonsave() {
		btnSave.click();
	}

	public void menuAddDoctor() {
		MenuAddDoctor.click();
	}
}
